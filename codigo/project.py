#!/usr/bin/env python
# -*- coding: utf-8 -*-

## @package project
# Documentación del código principal
import numpy as np
import cv2, cv
import os, math
from operator import itemgetter

refPt = []
cropping = False
points = []
distance = []
aux = []
clone = None


## Funcion que obtiene los puntos mas extremos, de forma vertical u horizontal
# @param type_m Tipo del analisis [h]orizontal o [v]ertical
# @param border_point Conjunto de puntos que apuntan al borde de la imagen
# @return Lista que contiene 2 puntos, el máximo y el mínimo
def get_max_point(type_m, border_point):
    dic = {}

    if type_m == 'v':
        border_point_x = min(border_point, key=itemgetter(1))
        border_point_y = max(border_point, key=itemgetter(1))
        return (border_point_x, border_point_y)

    elif type_m == 'h':
        key_sort = 1
        comparison_axis = 0

    for borde in border_point:
        if str(borde[key_sort]) not in dic:
            dic[str(borde[key_sort])] = []
        dic[str(borde[key_sort])].append(borde)

    points = [(0, 0), (0, 0)]

    for key_dic, puntos_dic in dic.items():

        maximo_filter = max(puntos_dic, key=itemgetter(comparison_axis))
        minimo_filter = min(puntos_dic, key=itemgetter(comparison_axis))

        posible_maximo = abs(maximo_filter[comparison_axis] - minimo_filter[comparison_axis])

        if abs(points[0][comparison_axis] - points[1][comparison_axis]) < posible_maximo:
            points = [maximo_filter, minimo_filter]

    return points


## Función que realiza segmentacion de una imagen en base a un limite
# @param image Imagen a segmentar
# @return La imagen y la segmentación
def segmentation_color(image):
    # Limites de colores (low,high)

    boundaries = [([0, 0, 0], [255, 255, 15])]

    for (lower, upper) in boundaries:
        lower = np.array(lower, dtype="uint8")
        upper = np.array(upper, dtype="uint8")

        mask = cv2.inRange(image, lower, upper)
        output = cv2.bitwise_and(image, image, mask=mask)

        return [image, output]


# Función que realiza el filtrado canny y recibe una imagen como parametro
# @param image Imagen a realizarle el filtro canny
# @param sigma Valor sigma
def auto_canny(image, sigma=0.43):
    v = np.median(image)

    # Valor de corte minimo
    lower = int(max(180, (1.0 - sigma) * v))
    # Valor de corte Maximo
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    return edged


## Funcion que convierte de pixel a cm
# @param pixels Cantidad de pixeles a convertir
# return Cantidad de CMs que equivalen los pixeles
def pixel_to_cm(pixels):
    return pixels * 2.54 / 96

## Función que obtiene 3 medidas de la imagen entregada un largo y dos anchos
# @param image Imagen a analizar
# @param name Nombre para la ventana de imagen a mostrar
# return Largo, y los dos anchos.
def get_measure(image, name):
    im_y, im_x = image.shape

    x, y = (0, 0)
    xwidth, yheight = (im_x - 1, im_y - 1)

    maximo = get_points(y, yheight, x, xwidth, image)

    cv2.circle(image, maximo[0], 8, (255, 255, 255), -1)
    cv2.circle(image, maximo[1], 8, (255, 255, 255), -1)

    cv2.line(image, maximo[0], maximo[1], (255, 0, 0), 2)

    largo = pixel_to_cm(math.sqrt((maximo[1][0] - maximo[0][0]) ** 2 + (maximo[1][1] - maximo[0][1]) ** 2))
    aux = maximo

    inicio = aux[0][1]
    tercio = (aux[1][1] - aux[0][1]) / 3

    maximo = get_points(aux[0][1], inicio + tercio, 0, im_x, image)

    cv2.circle(image, maximo[0], 8, (255, 255, 255), -1)
    cv2.circle(image, maximo[1], 8, (255, 255, 255), -1)

    cv2.line(image, maximo[0], maximo[1], (255, 0, 0), 2)

    # Ancho situado en la seccion plantar
    ancho_1 = pixel_to_cm(math.sqrt((maximo[1][0] - maximo[0][0]) ** 2 + (maximo[1][1] - maximo[0][1]) ** 2))

    maximo = get_points(inicio + tercio + tercio, inicio + tercio + tercio + tercio, 0, im_x, image)

    cv2.circle(image, maximo[0], 8, (255, 255, 255), -1)
    cv2.circle(image, maximo[1], 8, (255, 255, 255), -1)

    cv2.line(image, maximo[0], maximo[1], (255, 0, 0), 2)

    # Ancho localizdo en la seccion del talon
    ancho_2 = pixel_to_cm(math.sqrt((maximo[1][0] - maximo[0][0]) ** 2 + (maximo[1][1] - maximo[0][1]) ** 2))

    image2 = cv2.resize(image, (0, 0), fx=0.4, fy=0.4)
    cv2.imshow(name, image2)

    return [largo, ancho_1, ancho_2]


## Funcion que obtiene los puntos extremos de forma vertical u horizontal dependiendo de la zona seleccionada
# @param y Coordenada Y
# @param y_max Maximo de la coordenada Y
# @param x Coordenada X
# @param x_max Maximo de la coordenada Y
# @param image Imagen
# @return Punto maximo horizontal o vertical
def get_points(y, y_max, x, x_max, image):
    border_point = []

    roi = np.fliplr(np.rot90(image[y:y_max, x:x_max], 3))

    xroi, yroi = roi.shape

    for _xroi in xrange(xroi):
        for _yroi in xrange(yroi):
            if roi[_xroi][_yroi] == 255:
                border_point.append((_xroi + x, _yroi + y))

    if xroi <= yroi:
        maximo = get_max_point('v', border_point)
    else:
        maximo = get_max_point('h', border_point)

    return maximo


## Función que compara las medidas (altura y dos anchos ) de una imagen con otra
# @param measure Medidas del pie a analisar
# @param path Ruta de la carpeta donde se encuentran las imagenes a comparar
# @return Resultados de las comparaciones
def compare(measure, path):
    result = []
    listing = os.listdir(path)
    for i, path2 in enumerate(listing):
        if path2.endswith('.jpg'):
            full_path = os.path.join(path, path2)
            image = cv2.imread(full_path)
            image = get_image_border(image)
            measure2 = get_measure(image, str(i))

            print "-------------------" + path2 + "-------------------------------"

            print measure
            print measure2

            print "-----------------------------------------------------------\n"

            if (float(measure2[0] - measure[0]) < 3.0 and float(measure[0]) < float(measure2[0])) and (
                            float(measure2[1] - measure[1]) < 3 and float(measure[1]) < float(measure2[1])) and (
                            float(measure[2] - measure2[2]) < 3 and float(measure[2]) < float(measure2[2])):
                result.append(path2)

    return result


## Función que procesa la imagen para obtener sus bordes
# @param image Imagen a obtener sus bordes
# @return Imagen con el filtro Canny aplicado
def get_image_border(image):
    resize_image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)

    blurred_image = cv2.blur(resize_image, (40, 40))

    segmented_image = segmentation_color(blurred_image)

    gray_image = cv2.cvtColor(segmented_image[1], cv2.COLOR_BGR2GRAY)

    canny = auto_canny(gray_image)

    return canny


if __name__ == "__main__":

    pie = cv2.imread('../pruebas/pie1.jpg')

    pie = get_image_border(pie)

    clone = pie.copy()

    measure = get_measure(clone, 'pie')

    result = compare(measure, '../pruebas/calzados')

    print "-------------------- Calzados adecuados -------------------"

    print result

    while True:

        key = cv2.waitKey(1) & 0xFF

        if key == ord("c"):
            break

    cv2.destroyAllWindows()
