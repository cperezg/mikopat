var project_8py =
[
    [ "auto_canny", "project_8py.html#a15755d245b8a82b9b827656dbec53959", null ],
    [ "compare", "project_8py.html#adcbf219e15c5806e2f77c4371b5334fb", null ],
    [ "get_image_border", "project_8py.html#a871a1807b83cd20c5798d02320d1355d", null ],
    [ "get_max_point", "project_8py.html#a3c291580d04421b4c294a53a4ec0bdd6", null ],
    [ "get_measure", "project_8py.html#ae2d0a0debbaeb384bf4c3d889dc72a91", null ],
    [ "get_points", "project_8py.html#a7119ceff524bb71989893bfbac6a91dd", null ],
    [ "pixel_to_cm", "project_8py.html#af0b3a3fe3d51dcc080e684849ed50ba2", null ],
    [ "segmentation_color", "project_8py.html#afcab314ca56a7d07f3f9490f87c96a93", null ],
    [ "aux", "project_8py.html#a58409b119e2fced85ecc83fcdbc4af1c", null ],
    [ "clone", "project_8py.html#ac7a9d7884c6cf692ed720a261bfa6987", null ],
    [ "cropping", "project_8py.html#ac689b727311abab85202e77f2f9ea336", null ],
    [ "distance", "project_8py.html#ae00a101a5c02a4ca3e5efc14cd92935c", null ],
    [ "key", "project_8py.html#a82c79fb990cff066572254360b869600", null ],
    [ "measure", "project_8py.html#ac3748a13303ca1c1f8f4f7168825226d", null ],
    [ "pie", "project_8py.html#a86fcd4a91857db749a173f2e5207ed81", null ],
    [ "points", "project_8py.html#afb7f3f8478c8781c5590b9c2a65027cd", null ],
    [ "refPt", "project_8py.html#a57a886fd03a9f205b78cf7c233fc1a4d", null ],
    [ "result", "project_8py.html#a9986c45601bc20bd1dc3679abdd8dd60", null ]
];